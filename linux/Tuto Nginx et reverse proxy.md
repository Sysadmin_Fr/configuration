# Reverse proxy Nginx
## Les bases
### Le fichier de config
Nginx se base sur `UN` fichier de conf. En soit c'est comme ça qu'il le voit donc c'est vrai. Mais pour nous humains on va en utiliser plusieurs parce que c'est plus simple pour s'organiser.

Le fichier de base est /etc/nginx/nginx.conf (pas très compliqué)

On peux inclure des fichiers ou l'ensemble d'un dossier avec `include` comme sur cet exemple.

`include /etc/nginx/modules-enabled/*.conf`

Note si vous faites référence à un fichier de conf depuis un autre pensez à toujours mettre le chemin relatif **par rapport à nginx.conf**.

### Lire nginx.conf
Le fichier se lit de haut en bas (normal) et nginx prendra toujours ses décisions en suivant cet ordre de lecture.

Si tu ne comprends pas pourquoi nginx ne fait pas ce que tu veux, lit le fichier ligne par ligne et met toi à sa place.

### Les blocks
Nginx fonctionne avec des blocs, des blocs peuvent s'imbriquer voici un exemple d'un serveur http

```
http {
  ...
  server {
    ...
  }
  ...
}
```

*Vous avez vu ça se lit tout seul !*

Les blocs peuvent être de différents types, un autre que nous allons aborder est `stream`

## Nginx comme proxy HTTP
### HTTP

Considérons un reverse proxy HTTP simple.

```
http {
  server {
    listen 80;
    server_name www1.myapp.com;
    location / {
      proxy_pass http://ip_srv_backend;
    }
  }
}
```

La lecture est assez simple :
- le protocole que parlera nginx est http
- sur ce protocole un serveur est utilisé
- le serveur écoute sur le port 80
- le serveur répondra au nom de domaine www1.myapp.com *on verra un peu plus là-dessus mais ce n'est pas le sujet*
- pour toutes ressources demandées commençant pas / (donc tout) la requête est transmise à une ip d'un serveur web en backend.

Et là j'aimerais qu'on soit alignés sur le fonctionnement d'un proxy HTTP.

Nous avons donc un client (C), un reverse proxy (RP) et un serveur web (W).

- `C` veut atteindre le site web
- `C` envoie une requête au `RP`
- `RP` regarde la ressource demandée
- `RP` requête `W` en lui indiquant la ressource souhaitée
- `W` répond au `RP`
- `RP` renvoie la ressource à `C`

### Multi domaines

Si l'on veut gérer plusieurs noms de domaine il suffit de rajouter un bloc server.

```
http {
  server {
    listen 80;
    server_name www1.myapp.com;
    location / {
      proxy_pass http://ip_srv_backend_1;
    }
  }
  server {
    listen 80;
    server_name www2.myapp.com;
    location / {
      proxy_pass http://ip_srv_backend_2;
    }
  }
}
```

Comme pour le précédent exemple on pourrait se demander qu'elle est le serveur qui va répondre si le domaine demandé n'est ni www1.myapp.com, ni www2.myapp.com.

Plutôt que de rentrer dans les détails de suite, une des bonnes pratiques à faire et de mettre un serveur par défaut cela se fait comme ça en général.

```
http {
  server {
    listen 80;
    server_name _;
    root /var/www/html/index.html;
  }
  server {
    listen 80;
    server_name www1.myapp.com;
    location / {
      proxy_pass http://ip_srv_backend_1;
    }
  }
  server {
    listen 80;
    server_name www2.myapp.com;
    location / {
      proxy_pass http://ip_srv_backend_2;
    }
  }
}
```

Il est important que le server répondant à "**_**" soit le premier car c'est celui par défaut qui sera utilisé si aucun autre ne correspond au domaine demandé.

### HTTPS
Voyons maintenant HTTPS, ma réponse est simple et elle s'appelle Certbot, en vrai je n'utilise que lui car il automatise tout et m'évite de me poser des questions de sécurité inutilement.

Mais vu que ce n'est pas une bonne réponse pour vous on va voir ça **rapidement** en reprenant l'exemple précédant, on souhaite mettre du SSL sur www1.myapp.com et www2.myapp.com mais pas sur le serveur par défaut pour des raisons évidentes. (on suppose que vous avez un certificat wildcard pour plus de simplicité)

```
http {
  server {
    listen 80;
    server_name _;
    root /var/www/html/index.html;
  }
  server {
    listen 443;
    server_name www1.myapp.com;
    ssl_certificate /path/to/ssl//files/fullchain.pem;
    ssl_certificate_key /path/to/ssl//files/privkey.pem;
    ssl on;
    ssl_session_cache  builtin:1000  shared:SSL:10m;
    ssl_protocols TLSv1.2;
    ssl_ciphers HIGH:!aNULL:!eNULL:!EXPORT:!CAMELLIA:!DES:!MD5:!PSK:!RC4;
    ssl_prefer_server_ciphers on;
    location / {
      proxy_pass http://ip_srv_backend_1;
    }
  }
  server {
    listen 443;
    server_name www2.myapp.com;
    ssl_certificate /path/to/ssl//files/fullchain.pem;
    ssl_certificate_key /path/to/ssl//files/privkey.pem;
    ssl on;
    ssl_session_cache  builtin:1000  shared:SSL:10m;
    ssl_protocols TLSv1.2;
    ssl_ciphers HIGH:!aNULL:!eNULL:!EXPORT:!CAMELLIA:!DES:!MD5:!PSK:!RC4;
    ssl_prefer_server_ciphers on;
   location / {
     proxy_pass http://ip_srv_backend_2;
    }
  }
}
```

Bon pour être honnête en fait je ne suis pas au point sur TLS, les ciphers et tout ça donc je vous invite à en apprendre plus sur des sites plus spécialisé comme celui-là https://angristan.fr/configurer-https-nginx/

Je peux quand même vous suggérez d'éviter ce qu'on a fait au-dessus, on a écrit deux fois la même config et c'est pas beau, ni facilement maintenable. On va utiliser des include pour avoir un seul fichier de conf qui traitera du SSL.

```
http {
  server {
    listen 80;
    server_name _;
    root /var/www/html/index.html;
  }
  server {
    listen 443;
    server_name www1.myapp.com;
    include ./ssl.conf
    location / {
      proxy_pass http://ip_srv_backend_1;
    }
  }
  server {
    listen 443;
    server_name www2.myapp.com;
    include ./ssl.conf
    location / {
      proxy_pass http://ip_srv_backend_2;
    }
  }
}
```

/etc/nginx/ssl.conf
```
ssl_certificate /path/to/ssl//files/fullchain.pem;
ssl_certificate_key /path/to/ssl//files/privkey.pem;
ssl on;
ssl_session_cache  builtin:1000  shared:SSL:10m;
ssl_protocols TLSv1.2;
ssl_ciphers HIGH:!aNULL:!eNULL:!EXPORT:!CAMELLIA:!DES:!MD5:!PSK:!RC4;
ssl_prefer_server_ciphers on;
```

C'est plus propre vous ne trouvez pas ?

On voit ici que c'est le reverse proxy qui se charge de l'encapsulation HTTPS. Le trafic vers le serveur backend est en clair. Comme nous l'avons vu le `proxy_pass` se base sur le chemin de la ressource demandée pour déterminer le serveur à atteindre. Seulement dans une requête HTTPS la ressource demandée est chiffrée. Le serveur **doit** désencapsuler le SSL pour pouvoir répondre. **La seule information visible dans une requête HTTPS est le SNI**. Pourquoi ? Parce qu'elle est renseignée dans la trame TCP qui est envoyée avant d'établir la connexion HTTPS.

Qu'en est-il si l'on veut que ce soit le serveur backend qui se charge de l'encapsulation HTTPS ? Eh bien nous allons voir ça de suite !

## Nginx comme proxy TCP

Comme dit précédemment si nous ne désencapsulons pas la requête HTTPS la seule information disponible est le SNI. Nous allons être obligé de jouer avec ça pour faire notre config. **Au diable le bloc http {} place à stream {}**

```
stream {
  map $ssl_preread_server_name $name {
    www1.myapp.com www1_backend;
    www2.myapp.com www2_backend;
    default https_default_backend;
  }
  upstream www1_backend {
    server ip_srv_backend_1:443;
  }
  upstream www2_backend {
    server ip_srv_backend_2:443;
  }
  upstream https_default_backend {
    server 127.0.0.1:443;
  }
  server {
    listen 192.168.0.5:443;
    proxy_pass $name;
    ssl_preread on;
  }
}
```

Décortiquons ça !

- on déclare que l'on va utiliser un bloc `stream`, ça n'a l'air de rien mais plutôt que d'examiner des requêtes HTTP nginx va maintenant étudier les trames TCP !
- on utilise ensuite un bloc `map`, ce bloc prend en argument deux variables, `$ssl_preread_server_name` qui est ni plus ni moins le SNI dont nous avons parlé et `$name` qui est le `upstream` vers qui on va rediriger la trame. Ainsi www1 ira vers le upstream www1 et www2 vers le upstream www2 (merci cap'tain obvious) on notera le `default` qui permet de récupérer tout le reste et de le rediriger vers `https_default_backend`
- on déclare l'ensemble des upstreams et les IP de destinations.
- enfin on déclare le bloc `server` qui indique qu'un serveur stream va tourner.
- Son port d'écoute est sur l'ip d'une interface au port 443
- le serveur fera proxy vers le serveur $name qui a été déterminé dans le map
- et surtout on active le ssl_preread, c'est un peu la base de ce qu'on est en train de mettre en place...

On gère les requêtes vers des noms de domaines inconnus d'une manière particulière ici. Comme on dit que l'écoute se fait sur `192.168.0.5` cela signifie que la boucle locale (127.0.0.1) n'est pas en écoute, on redirige pourtant le `default` vers celle-ci afin que rien ne soit desservie. Le navigateur affichera une jolie erreur indiquant que la connexion a échoué. Libre à vous de gérer ces demandes particulières comme vous le souhaitez.

## Nginx, le mix de l'été!
Bon, et si on veut faire les deux à la fois ? Par exemple www1.myapp.com est un serveur Apache gérant lui-même son SSL et www2.myapp.com est encapsulé au niveau du Nginx.

Eh bien bien c'est possible mais il faut utiliser un trick ! Pourquoi ?

Voyons cette configuration:

```
stream {
  server {
    listen 443;
  }
}
http {
  server {
    listen 443;
  }
}
```

Vous comprenez mieux ? Si Nginx doit écouter uniquement sur le port 443 pour le protocole HTTP et TCP comment lorsqu'il reçoit la trame sera-t-il capable de savoir lequel utiliser ?

Et c'est là qu'intervient le trick ;) on va tout écouter sur le 443 en tcp et si c'est un nom de domaine qui doit être traité dans en HTTP et bien on va le rediriger vers un port du localhost.

```
stream {
  map $ssl_preread_server_name $name {
    www1.myapp.com www1_backend;
    www2.myapp.com www2_backend;
    default https_default_backend;
  }
  upstream www1_backend {
    server ip_srv_backend_1:443;
  }
  upstream www2_backend {
    server `127.0.0.1:4443`;
  }
  upstream https_default_backend {
    server 127.0.0.1:443;
  }
  server {
    listen 192.168.0.5:443;
    proxy_pass $name;
    ssl_preread on;
  }
}
http {
  server {
    listen 80;
    server_name _;
    root /var/www/html/index.html;
  }
  server {
    listen `127.0.0.1:4443`;
    server_name www2.myapp.com;
    include ./ssl.conf
    location / {
      proxy_pass http://ip_srv_backend_2;
    }
  }
}
```

Et voilà !

Si une requête arrive pour www1.myapp.com elle sera seulement prélu mais pas désencapsuler et sera rediriger vers ip_srv_backend_1

Si c'est à destination de `www2.myapp.com` elle sera redirigé sur le port 4443 de la boucle locale (127.0.0.1) et c'est un serveur `http` qui répondra et gérera le SSL.

Si c'est un domaine inconnu sur le port 80, il tombera sur index.html, si c'est le port 443 ça sera une erreur sur le navigateur.

Évidemment les confs de ce tuto sont minimalistes, regardez les exemples de base de nginx pour voir les directives intéressantes comme les logs par exemple !

J'espére que vous aurez appris des choses dans ce tuto n'hésitez pas à me faire des retours à contact [ at ] nonot.eu, et si vous avez des suggestions/corrections/améliorations aussi.

PS: On a encore beaucoup à dire mais je m'arrête là, on peux notamment évoquer le fait que le proxy TCP masque la vraie adresse IP du client, qu'il faudrait splitter tout ça en différent fichier, ...
